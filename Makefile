1;95;0cDEPS = *.tex *.bib

all: resume.pdf resume_pro.pdf resume_pro_onepage.pdf cover_letter.pdf

cover_letter.pdf: cover_letter.tex
	pdflatex cover_letter.tex
	cp cover_letter.pdf "CoverLetterSimoneAtzeni.pdf"
	rm -f *.ps *.dvi *~ \#*\#
	rm -rf *.aux *.bbl *.bcf *.blg *.dvi *.log *.rel *.toc *.out *.synctex.gz auto

resume.pdf: resume.tex $(DEPS)
	pdflatex resume.tex
	bibtex resume
	pdflatex resume.tex
	pdflatex resume.tex
	cp resume.pdf "ResumeResearchSimoneAtzeni.pdf"
	rm -f *.ps *.dvi *~ \#*\#
	rm -rf *.aux *.bbl *.bcf *.blg *.dvi *.log *.rel *.toc *.out *.synctex.gz auto

resume_pro.pdf: resume_pro.tex $(DEPS)
	pdflatex resume_pro.tex
	bibtex resume_pro
	pdflatex resume_pro.tex
	pdflatex resume_pro.tex
	cp resume_pro.pdf "ResumeSimoneAtzeniLong.pdf"
	rm -f *.ps *.dvi *~ \#*\#
	rm -rf *.aux *.bbl *.bcf *.blg *.dvi *.log *.rel *.toc *.out *.synctex.gz auto

resume_pro_onepage.pdf: resume_pro_onepage.tex $(DEPS)
	pdflatex resume_pro_onepage.tex
	# bibtex resume_pro_onepage
	pdflatex resume_pro_onepage.tex
	pdflatex resume_pro_onepage.tex
	cp resume_pro_onepage.pdf "ResumeSimoneAtzeni.pdf"
	rm -f *.ps *.dvi *~ \#*\#
	rm -rf *.aux *.bbl *.bcf *.blg *.dvi *.log *.rel *.toc *.out *.synctex.gz auto

clean:
	rm -f resume.pdf resume_pro.pdf resume_pro_onepage.pdf cover_letter.pdf *.ps *.dvi *~ \#*\#
	rm -rf *.aux *.bbl *.bcf *.blg *.dvi *.log *.rel *.toc *.out *.synctex.gz auto
